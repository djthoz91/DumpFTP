#!/bin/sh
#*************************************************************************#
# backup du ftp
# Script by Nicolas DA COSTA
# admin : www.ndacostaleitao.info
# e-mail: 
#*************************************************************************#
# design/developpement/consulting: https://www.ndacostaleitao.info
# 
#*************************************************************************#

# ce script necessite le logiciel lftp
#
#************************** Declaration de variables **********************

# nom du serveur ou son IP
server=""

# login du FTP
user=""

# mot de pass du FTP
pass=""

# repertoire ou la sauvegarde sera stockee
stock="" 

# repertoire temporaire
temp="/tmp_ftp/temp"

# repertoire du ftp qui sera sauvegarde
cible="/"

# pour le datage des fichiers et des logs
jour=$(date +%d-%m-%y)
dates="$(date +"%d/%m/%y"/%H:%M)"

# le nom de votre archives et du log associe
nom_archive=""

# archive du ftp
archive="/$stock/archive_de_"${nom_archive}"_du_"${jour}".tar.gz" 

# fichier de log
log="/var/log/save_ftp/save_ftp_"${nom_archive}"_du_"${jour}".log" 

# fichier de log LFTP
log2="/var/log/save_ftp/save_ftp_lftp"${nom_archive}"_du_"${jour}".log" 

# email du destinataire
EMAILID=root
#**************************Fin de declaration des variables **************
#**************************Fonctions********************************
##FONCTION GESTION CODE RETOUR
gestion_retour() {
        if [ $1 -eq 0 ]; then
                echo -e "\033[1;32m ... OK \033[0m"
        else
                echo " ... ERREUR CODE $? " >> $log
		  mail -s "Echec BACKUP_"${nom_archive}"_du_"${jour}"" "$EMAILID" <$log
                exit 1
        fi
}
##FONCTION VERIFICATION
prestart() {
	if [ -d "$temp" ]; then            
                echo -e "\033[1;32m Le répertoire "temp" existe déjà...Préparation du téléchargement \033[0m" && cd $temp                                  
	else
	echo "Le répertoire temp n'existe pas ! arrêt de la sauvegarde..." >> $log && gestion_retour +1
	fi 
}
##FONCTION EFFACEMENT
erase() {
#ouvre le repertoire temporaire #vide le repertoire temporaire
	if [ "$(ls -A /tmp_ftp/temp)" ]; then
		echo -e "\033[1;32m Non vide \033[0m"
		cd $temp && find . -name . -o -prune -exec rm -rf -- {} + && gestion_retour $?
	else
		echo -e "\033[1;32m Vide \033[0m"
	fi
}
#**************************debut du script********************************

prestart
echo "[`date +"%d/%m/%y"-"%H:%M"`] Debut du telechargement de la sauvegarde vers le serveur" >$log

# lance la commande de transfert + log
lftp -u $user,$pass -e "mirror delete-first -c --parallel=2 $cible -v --log=$log2 ; quit" $server && gestion_retour $?
echo "[`date +"%d/%m/%y"-"%H:%M"`] Fin du telechargement de la sauvegarde vers le serveur" >>$log

echo "[`date +"%d/%m/%y"-"%H:%M"`] Creation de l archive" >>$log

# creation de l'archive
tar cfz "${archive}" "${temp}" && gestion_retour $?
echo "[`date +"%d/%m/%y"-"%H:%M"`] Archive OK" >>$log

echo "[`date +"%d/%m/%y"-"%H:%M"`] Suppression du repertoire temporaire" >>$log
erase
echo "[`date +"%d/%m/%y"-"%H:%M"`] Suppression OK" >>$log

echo "[`date +"%d/%m/%y"-"%H:%M"`] Sauvegarde du site $nom_archive OK" >>$log

# envoie du raport 
mail  -s "BACKUP_"${nom_archive}"_du_"${jour}"" "$EMAILID" <$log

# suprimmer les sauvegardes >35 days
echo [$dates] Supression des sauvegarde superieur a  35 jours >>$log
cd $stock && find . -name '*.tar.gz' -mtime +35 | xargs rm && gestion_retour $?

#anonymisation des donnees
sed -i -e "s/$pass/xxxxxx/g;s/$user/anonymous/g" $log2 && gestion_retour $?
#**************************fin du script**********************************

# Ce script peut etre redistribue a volonte et modifie a volonte
